<div class="modal fade bd-SAS-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 my-auto">
                            <img data-src="{{asset('img/sas.png')}}" class="lazy modal-title w-100" id="exampleModalLabel" alt="">
                        </div>
                        <div class="col-md-8">
                            <p class="mb-0">Spice Application Systems Ltd (SAS) specializes in using electrostatic technology to apply flavourings, coatings, powders, additives, vitamins, spices and oils to a wide range of food and pharmaceutical products.</p>
                        </div>
                    </div>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/sas1.png')}}" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Electrostatic Seasoner</h4>
                        <p class="card-text">Spice Application Systems Ltd. (SAS) specialises in using electrostatic technology to applay flavourings, coatings, powders, additives, vitamins, spices and oils to a wide range of food and pharmaceutical products.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/sas2.png')}}" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Drum</h4>
                        <p class="card-text">
                            <ul>
                                <li>Stainless steel 316/304</li>
                                <li>Electronic drive motor</li>
                                <li>On castors</li>
                                <li>Adjustable drum speed</li>
                                <li>Adjustable drum angle</li>
                                <li>Sizes from 1mtr-2.5mtr/diam 0.4mtr-1mtr</li>
                            </ul>
                        </p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/sas3.png')}}" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Screw Feeder</h4>
                        <p class="card-text">
                            <ul>
                                <li>Twin screw</li>
                                <li>0.5kg-65kg</li>
                                <li>Remote controls</li>
                                <li>Stainless steel 316/304</li>
                                <li>Designed to complement the SAS drum</li>
                            </ul>
                        </p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/sas4.png')}}" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Oil Spray System</h4>
                        <p class="card-text">
                            <ul>
                                <li>25ltr-100ltr hopper</li>
                                <li>Compressed air flow system</li>
                                <li>Feeds up to four spray heads</li>
                                <li>On castors</li>
                                <li>Electronic pump</li>
                                <li>Stainless steel 316/304</li>
                                <li>Designed to fit the SAS drum</li>
                            </ul>
                        </p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/sas5.png')}}" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">In-Feed Tray</h4>
                        <p class="card-text">
                            <ul>
                                <li>0.75mtr-3mtr x 450mm</li>
                                <li>Adjustable height</li>
                                <li>Adjustable speed</li>
                                <li>Stainless steel 316/304</li>                              
                            </ul>
                        </p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
