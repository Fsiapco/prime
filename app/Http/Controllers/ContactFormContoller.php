<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\Contact;

class ContactFormContoller extends Controller
{
    public function sendMail(Request $request){
        $contact = (object) [
                'name' => $request->name, 
                'email' => $request->email,
                'number' => $request->number, 
                'message' => $request->message
            ];
        \Mail::to('info@primeopusinc.com')->send(new \App\Mail\Contact($contact));
        return 1;
    }
}
